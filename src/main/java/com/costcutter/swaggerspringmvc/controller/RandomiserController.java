package com.costcutter.swaggerspringmvc.controller;

import com.costcutter.swaggerspringmvc.model.Randomiser;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "Randomiser Controller", description = "v1")
public class RandomiserController {
    
    @RequestMapping(value = "get-random-data/{limit}", method = RequestMethod.GET)
    public Randomiser getRandomData(@PathVariable int limit) {
        return new Randomiser(limit);
    }
}
