package com.costcutter.swaggerspringmvc.model;

import java.util.Random;

public class Randomiser {
    private final boolean randomBoolean;
    private final int randomNumber;
    
    public Randomiser(int limit) {
        randomBoolean = new Random().nextBoolean();
        randomNumber = new Random().nextInt(limit + 1); // +1 as parameret is exclusive
    }

    public boolean isRandomBoolean() {
        return randomBoolean;
    }

    public int getRandomNumber() {
        return randomNumber;
    }
}
